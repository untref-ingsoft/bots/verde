require "#{File.dirname(__FILE__)}/../lib/routing"
require "#{File.dirname(__FILE__)}/../lib/version"
require "#{File.dirname(__FILE__)}/tv/series"
require "#{File.dirname(__FILE__)}/job_offers/job_offers"
require "#{File.dirname(__FILE__)}/job_offers/job_offers_presenter"
require "#{File.dirname(__FILE__)}/job_offers/job_offers_service"

class Routes
  include Routing

  on_message '/start' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{message.from.first_name}")
  end

  on_message_pattern %r{/say_hi (?<name>.*)} do |bot, message, args|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{args['name']}")
  end

  on_message '/stop' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Chau, #{message.from.username}")
  end

  on_message '/developer' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'emilio')
  end

  on_message '/time' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "La hora es, #{Time.now}")
  end

  on_message '/tv' do |bot, message|
    kb = [Tv::Series.all.map do |tv_serie|
      Telegram::Bot::Types::InlineKeyboardButton.new(text: tv_serie.name, callback_data: tv_serie.id.to_s)
    end]
    markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)

    bot.api.send_message(chat_id: message.chat.id, text: 'Quien se queda con el trono?', reply_markup: markup)
  end

  on_message '/busqueda_centro' do |bot, message|
    kb = [
      Telegram::Bot::Types::KeyboardButton.new(text: 'Compartime tu ubicacion', request_location: true)
    ]
    markup = Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard: kb)
    bot.api.send_message(chat_id: message.chat.id, text: 'Busqueda por ubicacion', reply_markup: markup)
  end

  on_location_response do |bot, message|
    response = "Ubicacion es Lat:#{message.location.latitude} - Long:#{message.location.longitude}"
    puts response
    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_response_to 'Quien se queda con el trono?' do |bot, message|
    response = Tv::Series.handle_response message.data
    bot.api.send_message(chat_id: message.message.chat.id, text: response)
  end

  on_message '/version' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: Version.current)
  end

  on_message '/offers_list' do |bot, message|
    offers_data = JobOffersService.new.fetch_offers_list
    offers = JobOffers.new.offers_list(offers_data)
    response = JobOffersPresenter.present_offers_list(offers)
    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_message_pattern %r{/offer_(?<id>\d+)} do |bot, message, args|
    offers_data = JobOffersService.new.fetch_offers_list
    offer = JobOffers.new.offer_detail(args['id'], offers_data)
    response = JobOffersPresenter.present_offer_detail(offer)
    bot.api.send_message(chat_id: message.chat.id, text: response, parse_mode: 'Markdown')
  end

  on_message_pattern %r{/search (?<param>.+)} do |bot, message, args|
    search_parameter = args['param']
    search_results = JobOffersService.new.search_by_title(search_parameter)
    matched_offers = JobOffers.new.search_results(search_results)
    response = JobOffersPresenter.present_offers_list(matched_offers)
    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_message '/search' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'You must specify a search parameter')
  end

  on_message_pattern %r{/email_offer (?<id>\d+) (?<email>.+)} do |bot, message, args|
    offer_id = args['id']
    email = args['email']
    offers_data = JobOffersService.new.email_offer(offer_id, email)
    response = JobOffers.new.email_offer(offers_data)
    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_message_pattern %r{/email_offer (?<id>\d+)$} do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'You must specify an email address')
  end

  on_message '/email_offer' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'You must specify an offer id and an email address')
  end

  on_message_pattern %r{/next (?<id>\d+)} do |bot, message, args|
    next_offers = args['id']
    next_results = JobOffersService.new.next_offers(next_offers)
    next_offers = JobOffers.new.next_offers(next_results)
    response = JobOffersPresenter.present_offers_list(next_offers)
    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_message '/next' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'You must specify an offer id. For example: /next 100')
  end

  default do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'Uh? No te entiendo! Me repetis la pregunta?')
  end
end
