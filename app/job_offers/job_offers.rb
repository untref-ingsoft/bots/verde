class JobOffers
  def offers_list(api_response)
    if api_response.is_a?(Hash) && api_response.key?('message')
      api_response['message']
    else
      api_response.map { |offer| offer }
    end
  end

  def offer_detail(id, api_response)
    api_response.find { |offer| offer['id'] == id.to_i }
  end

  def search_results(api_response)
    if api_response.is_a?(Array)
      api_response.map { |offer| offer }
    else
      api_response['message']
    end
  end

  def email_offer(api_response)
    api_response['message']
  end

  def next_offers(api_response)
    if api_response.is_a?(Array)
      api_response.map { |offer| offer }
    else
      api_response['message']
    end
  end
end
