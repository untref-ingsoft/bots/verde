class JobOffersPresenter
  def self.present_offers_list(offers)
    return offers if offers.is_a?(String)

    offers.map { |offer| "#{offer['title']} - /offer_#{offer['id']}" }.join("\n")
  end

  def self.present_offer_detail(offer)
    if offer.nil?
      'The requested offer does not exist'
    else
      "*#{offer['title']}*\nDescription: #{offer['description']}\nLocation: #{offer['location']}"
    end
  end
end
