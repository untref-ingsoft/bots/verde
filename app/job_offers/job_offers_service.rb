class JobOffersService
  URL = ENV['JOB_OFFERS_API_URL'].freeze

  def fetch_offers_list
    response = Faraday.get("#{URL}/job_offers/offers_list")
    JSON.parse(response.body)
  end

  def search_by_title(title)
    title = URI.encode_www_form_component(title)
    response = Faraday.get("#{URL}/job_offers/search?job_title=#{title}")
    if response.success?
      JSON.parse(response.body)
    else
      raise StandardError, "Request error: #{response.status}"
    end
  end

  def email_offer(offer_id, email)
    email = URI.encode_www_form_component(email)
    response = Faraday.get("#{URL}/job_offers/email_offer?offer_id=#{offer_id}&email=#{email}")
    if response.success?
      JSON.parse(response.body)
    else
      raise StandardError, "Request error: #{response.status}"
    end
  end

  def next_offers(offer_id)
    offer_id = URI.encode_www_form_component(offer_id)
    response = Faraday.get("#{URL}/job_offers/next?offer_id=#{offer_id}")
    if response.success?
      JSON.parse(response.body)
    else
      raise StandardError, "Request error: #{response.status}"
    end
  end
end
