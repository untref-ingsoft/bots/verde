require 'spec_helper'
require 'web_mock'
# Uncomment to use VCR
# require 'vcr_helper'

require "#{File.dirname(__FILE__)}/../app/bot_client"

def when_i_send_text(token, message_text)
  body = { "ok": true, "result": [{ "update_id": 693_981_718,
                                    "message": { "message_id": 11,
                                                 "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                                                 "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                                                 "date": 1_557_782_998, "text": message_text,
                                                 "entities": [{ "offset": 0, "length": 6, "type": 'bot_command' }] } }] }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def when_i_send_keyboard_updates(token, message_text, inline_selection)
  body = {
    "ok": true, "result": [{
      "update_id": 866_033_907,
      "callback_query": { "id": '608740940475689651', "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                          "message": {
                            "message_id": 626,
                            "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                            "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                            "date": 1_595_282_006,
                            "text": message_text,
                            "reply_markup": {
                              "inline_keyboard": [
                                [{ "text": 'Jon Snow', "callback_data": '1' }],
                                [{ "text": 'Daenerys Targaryen', "callback_data": '2' }],
                                [{ "text": 'Ned Stark', "callback_data": '3' }]
                              ]
                            }
                          },
                          "chat_instance": '2671782303129352872',
                          "data": inline_selection }
    }]
  }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def then_i_get_text(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544', 'text' => message_text }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def then_i_get_keyboard_message(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544',
              'reply_markup' => '{"inline_keyboard":[[{"text":"Jon Snow","callback_data":"1"},{"text":"Daenerys Targaryen","callback_data":"2"},{"text":"Ned Stark","callback_data":"3"}]]}',
              'text' => 'Quien se queda con el trono?' }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def simulate_job_offers_api(body, endpoint, query_params = {})
  url = "#{ENV['JOB_OFFERS_API_URL']}#{endpoint}"
  url += "?#{URI.encode_www_form(query_params)}" unless query_params.empty?

  stub_request(:get, url)
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

describe 'BotClient' do
  let(:token) { 'fake_token' }
  let(:job_offers_body) do
    [
      { id: 1, title: 'Ruby Dev', location: 'Baires, Argentina', description: 'Remote' },
      { id: 2, title: 'Node Dev', location: 'Rio, Brazil', description: 'Remote' }
    ]
  end
  let(:job_offers_body_empty) { { "message": 'There are no job offers available' } }
  let(:email_offer_body) { { "message": 'Email sent to email@test.com' } }
  let(:job_offers_body_no_match) { { "message": 'No similar offers found' } }

  after(:each) do
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /version message and respond with current version' do
    when_i_send_text(token, '/version')
    then_i_get_text(token, Version.current)
  end

  it 'should get a /say_hi message and respond with Hola Emilio' do
    when_i_send_text(token, '/say_hi Emilio')
    then_i_get_text(token, 'Hola, Emilio')
  end

  it 'should get a /start message and respond with Hola' do
    when_i_send_text(token, '/start')
    then_i_get_text(token, 'Hola, Emilio')
  end

  it 'should get a /stop message and respond with Chau' do
    when_i_send_text(token, '/stop')
    then_i_get_text(token, 'Chau, egutter')
  end

  it 'should get a /tv message and respond with an inline keyboard' do
    when_i_send_text(token, '/tv')
    then_i_get_keyboard_message(token, 'Quien se queda con el trono?')
  end

  it 'should get a "Quien se queda con el trono?" message and respond with' do
    when_i_send_keyboard_updates(token, 'Quien se queda con el trono?', '2')
    then_i_get_text(token, 'A mi también me encantan los dragones!')
  end

  it 'should get a /offers_list message and respond with the offers list' do
    simulate_job_offers_api(job_offers_body, '/job_offers/offers_list')
    when_i_send_text(token, '/offers_list')
    then_i_get_text(token, "Ruby Dev - /offer_1\nNode Dev - /offer_2")
  end

  it 'should get a /offers_list message and respond with a warning if there are no offers ' do
    simulate_job_offers_api(job_offers_body_empty, '/job_offers/offers_list')
    when_i_send_text(token, '/offers_list')
    then_i_get_text(token, 'There are no job offers available')
  end

  it 'should get a /email_offer <id> <email> message and respond with a success message' do
    simulate_job_offers_api(email_offer_body, '/job_offers/email_offer',
                            { offer_id: 1, email: 'email@test.com' })

    when_i_send_text(token, '/email_offer 1 email@test.com')
    then_i_get_text(token, 'Email sent to email@test.com')
  end

  it 'should get a /email_offer message and respond with a warning if no id is given' do
    when_i_send_text(token, '/email_offer')
    then_i_get_text(token, 'You must specify an offer id and an email address')
  end

  it 'should get a /email_offer <id> message and respond with a warning if no email is given' do
    when_i_send_text(token, '/email_offer 1')
    then_i_get_text(token, 'You must specify an email address')
  end

  it 'should get a /search + <paramater> message and respond with the matching offers list' do
    simulate_job_offers_api(job_offers_body, '/job_offers/search', { job_title: 'dev' })
    when_i_send_text(token, '/search dev')
    then_i_get_text(token, "Ruby Dev - /offer_1\nNode Dev - /offer_2")
  end

  it 'should get a /search + <paramater> message and respond with a warning if there are no matching offers ' do
    simulate_job_offers_api(job_offers_body_no_match, '/job_offers/search', { job_title: 'Scrum' })
    when_i_send_text(token, '/search Scrum')
    then_i_get_text(token, 'No similar offers found')
  end

  it 'should get a /search message and respond with a warning if no parameter is given' do
    when_i_send_text(token, '/search')
    then_i_get_text(token, 'You must specify a search parameter')
  end

  it 'should get a /next <id> message and respond with a warning if no nearby offers are found' do
    no_nearby_offers_body = { "message": "No nearby offers were found for location 'Baires, Argentina'" }
    simulate_job_offers_api(no_nearby_offers_body, '/job_offers/next', { offer_id: 1 })
    when_i_send_text(token, '/next 1')
    then_i_get_text(token, "No nearby offers were found for location 'Baires, Argentina'")
  end

  it 'should get a /next message and respond with a warning if no id is given' do
    when_i_send_text(token, '/next')
    then_i_get_text(token, 'You must specify an offer id. For example: /next 100')
  end

  it 'should get an unknown message message and respond with Do not understand' do
    when_i_send_text(token, '/unknown')
    then_i_get_text(token, 'Uh? No te entiendo! Me repetis la pregunta?')
  end
end
